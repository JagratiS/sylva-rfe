Sylva is open to any new technical contribution. Few steps are required before adding new features to the Sylva stack.

# Signing the CLA (Contributor License Agreement)

**For a company**, the first action before contributing to Sylva is to sign the Corporate Contributor Agreement as explained below:

Corporate CLA contributors signing Process :

1. Go to [this url](https://organization.lfx.linuxfoundation.org/foundation/a092M00001KWzi6QAD/project/a092M00001L16ZLQAZ/cla) , which goes directly to Sylva Project Corporate CLA signature page
2. Otherwise, the Corporate contributors will need to access the [main page](https://organization.lfx.linuxfoundation.org/), select **Linux Foundation Europe**, and then select **Sylva project**.

To access this page, the initial CLA Manager of the organization **requires a LFID account** and belong to the organization which they are contributing with, this can be achieved in their Individual Dashboard profile [here](https://openprofile.dev/).

In case of issues, they can open a ticket with our Support Team [here](https://jira.linuxfoundation.org/plugins/servlet/desk/portal/4/create/143) and the EasyCLA team will gladly assist them. For more information about the Corporate CLA process please refer to [this page](https://docs.linuxfoundation.org/lfx/easycla/v2-current/corporate-cla-managers/coordinate-signing-cla-and-become-initial-cla-manager).

**Each developer must be registered on EasyCLA by the CLA manager of its company before pushing new merge request to any project part of Sylva.**

**For individuals**, if you are not part of a company, or if your company did not sign the Corporate Contributor License Agreement, you can sign yourself the Individual Contributor License Agreement by opening a ticket [here](https://jira.linuxfoundation.org/plugins/servlet/desk/portal/4/create/143).

# Create a gitlab.com account

Sylva is hosted on gitlab.com, so you have to create a gitlab.com account to propose a new merge request.

## Create merge request from your gitlab account

Once you are sign in to gitlab.com, you can fork any sylva project in your gitlab.com group, work on your feature in a dedicated branch, and propose a merge request to the sylva project from your development branch.

# Propose enhancements or features

As an openscource community, we need to agree on the introduction of new enhancements or features that may significatively impact the stack. Any new feature should be discussed and reviewed with the sylva community. We need to understand the proposal and its technical impact on the stack.

Requests for enhancements and features are made with a merge request based on this [template document](RFE/template.md). The community will be able to discuss this proposal, how to implement and test it, during the review of the document.

# Discuss with the community

The two privileged ways to discuss with the sylva community are:

- via slack: sylva-projects.slack.com [invitation link](https://join.slack.com/t/sylva-projects/shared_invite/zt-1owzbom3p-MiSitCf~zJa8bFNoRjHKGQ)
- via gitlab issue: don't hesitate to create an issue in any sylva-porjects, we'll discuss on it


